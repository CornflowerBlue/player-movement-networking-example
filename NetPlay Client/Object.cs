﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace NetPlay_Client
{
    public class Object
    {
        public Texture2D Texture;
        public Vector2 Position;
        public bool IsMine = true;
        

        public virtual void Update(GameTime gt)
        {

        }

        public virtual void Draw(GameTime gt, SpriteBatch sb)
        {
            if (Texture == null)
                return;


            sb.Draw(Texture, Position, Color.White);
        }
    }
}
