﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace NetPlay_Client
{
    class Player : Object
    {
        public NetConnection Connection;
        public int ID;

        public float Speed = 330f;


        public Player()
        {
            Texture = Game1.PlayerTexture;
        }

        public override void Update(GameTime gt)
        {
            base.Update(gt);

            if (!IsMine)
                return;

            var kbState = Keyboard.GetState();

            var vel = new Vector2();
            if (kbState.IsKeyDown(Keys.W))
                vel.Y = -1;
            else if (kbState.IsKeyDown(Keys.S))
                vel.Y = 1;

            if (kbState.IsKeyDown(Keys.A))
                vel.X = -1;
            else if (kbState.IsKeyDown(Keys.D))
                vel.X = 1;

            Position += vel * Speed * (gt.ElapsedGameTime.Milliseconds / 1000f);
        }
    }
}
