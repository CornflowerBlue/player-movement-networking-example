﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NetPlay_Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace NetPlay_Client
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static Texture2D PlayerTexture;
        public List<Object> Objects = new List<Object>();

        static Queue<NetIncomingMessage> MessageQueue;
        NetClient Client;

        Player Me;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            MessageQueue = new Queue<NetIncomingMessage>();

            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            IsMouseVisible = true;
            // TODO: use this.Content to load your game content here

            PlayerTexture = Content.Load<Texture2D>("0");

            Me = new Player();
            Objects.Add(Me);
        }
        
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        
        private void JoinServer()
        {
            var npc = new NetPeerConfiguration("NetPlay");

            Client = new NetClient(npc);

            Client.RegisterReceivedCallback(new SendOrPostCallback(GotMessage));
            Client.Start();
            Client.Connect("localhost", 9970);
        }

        private void GotMessage(object state)
        {
            lock (MessageQueue)
            {
                var message = Client.ReadMessage();
                MessageQueue.Enqueue(message);
            }
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


            if (Keyboard.GetState().IsKeyDown(Keys.F1))
                JoinServer();

            if (Keyboard.GetState().IsKeyDown(Keys.F2))
                LeaveServer();


            UpdateNetworking(gameTime);
            UpdateGame(gameTime);
            

            base.Update(gameTime);
        }

        private void LeaveServer()
        {
            if (Client?.Status == NetPeerStatus.Running)
                Client.Disconnect("Bye");
        }

        private void UpdateNetworking(GameTime gameTime)
        {
            lock (MessageQueue)
            {
                for (int i = 0; i < MessageQueue.Count; i++)
                {
                    HandleServerMessage(MessageQueue.Dequeue());
                }
            }
        }

        private void UpdateGame(GameTime gameTime)
        {
            foreach (var g in Objects)
                g.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            foreach (var g in Objects)
                g.Draw(gameTime, spriteBatch);

            spriteBatch.End();


            base.Draw(gameTime);
        }

        // Networking methods
        private void HandleServerMessage(NetIncomingMessage message)
        {
            switch (message.MessageType)
            {
                case NetIncomingMessageType.StatusChanged:
                    //  Todo:
                    Debug.WriteLine($"{message.MessageType}: {Client.Status.ToString()}");
                    break;
                case NetIncomingMessageType.Data:
                    HandleDataPacketFromServer(message);
                    break;
                default:
                    Debug.WriteLine($"{message.MessageType}: {message.ReadString()}");
                    break;
            }
        }

        private void HandleDataPacketFromServer(NetIncomingMessage message)
        {
            var packetType = PacketTypes.Null;
            packetType = (PacketTypes)message.ReadInt32();

            switch (packetType)
            {
                case PacketTypes.Ping:
                    var pingId = message.ReadInt32();
                    SendPong(message.SenderConnection, pingId);
                    break;
                case PacketTypes.Pong:
                    // TODO Handle Ping/pong for server->client
                    break;
                case PacketTypes.Welcome:
                    Me.ID = message.ReadInt32();
                    break;
            }

            // If we don't read all the data in the packet
            if (message.Position != message.LengthBits)
                Debug.WriteLine($"Malformed packet detected! {packetType}");
        }

        private NetOutgoingMessage CreatePacket(PacketTypes msgType)
        {
            var m = Client.CreateMessage();
            m.Write((int)msgType);

            return m;
        }

        private void SendPong(NetConnection connection, int pingId)
        {
            var m = CreatePacket(PacketTypes.Pong);
            m.Write(pingId);
            connection.SendMessage(m, NetDeliveryMethod.ReliableOrdered, 31);
        }

        private void SendPing(NetConnection connection, int pingId)
        {
            var m = CreatePacket(PacketTypes.Ping);
            m.Write(pingId);
            connection.SendMessage(m, NetDeliveryMethod.ReliableOrdered, 31);
        }
    }
}
