﻿using Lidgren.Network;

namespace NetPlay_Common
{
    public class NetPlayer
    {
        public NetConnection Connection;

        public int ID;
        public float X, Y;

        public NetPlayer(NetConnection connection)
        {
            Connection = connection;
        }
    }
}
