﻿namespace NetPlay_Common
{
    public enum PacketTypes : int
    {
        Null = 0,
        Ping = 1, // SERVER CLIENT
        Pong = 2, // SERVER CLIENT
        ConnectionAdd = 3, // CLIENT
        ConnectionRemove = 4, // CLIENT
        Welcome = 5, // CLIENT
    }
}
