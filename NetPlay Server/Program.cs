﻿using Lidgren.Network;
using NetPlay_Common;
using System;
using System.Collections.Generic;
using System.Threading;

namespace NetPlay_Server
{
    class Program
    {
        const int TICKRATE = 16; // aim tickrate in ms
        const int PLAYERMAX = 16;

        static Queue<NetIncomingMessage> MessageQueue;

        static float GameTime; 
        static float c_GameTime;
        static float FpsTimer;
        static float Fps;
        static float c_Fps;
        
        static NetServer Server;

        static Dictionary<NetConnection, NetPlayer> Players;

        [STAThread()]
        static void Main(string[] args)
        {
            Players = new Dictionary<NetConnection, NetPlayer>(PLAYERMAX);
            MessageQueue = new Queue<NetIncomingMessage>();

            Server = new NetServer(new NetPeerConfiguration("NetPlay")
            {
                AcceptIncomingConnections = true,
                MaximumConnections = PLAYERMAX,
                Port = 9970
            });

            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            Server.RegisterReceivedCallback(new SendOrPostCallback(GotMessage));
            Server.Start();

            // Server Loop
            while (true)
            {
                var startTime = Environment.TickCount;

                GameLoop(GameTime - c_GameTime);
                c_Fps++;

                // Ensures our loop is at a fixed rate
                var elapTime = Environment.TickCount - startTime;
                if (TICKRATE > elapTime)
                {
                    Thread.Sleep(TICKRATE - elapTime);
                    elapTime = Environment.TickCount - startTime;
                }

                c_GameTime = GameTime;
                GameTime += elapTime;
                FpsTimer += elapTime;

                
                // FPS Timing
                if (FpsTimer > 1000)
                {
                    Fps = c_Fps;
                    c_Fps = 0;
                    FpsTimer = 0;

                    Console.Title = $"P: {Players.Count}, Tick: {GameTime - c_GameTime}, GT: {GameTime}, FPS: {Fps}";
                }
            }

            Console.Write("EXIT");
            Console.ReadKey();
        }

        private static void GotMessage(object state)
        {
            lock (MessageQueue)
            {
                var message = Server.ReadMessage();
                MessageQueue.Enqueue(message);
            }
        }

        private static void GameLoop(float dtMs)
        {
            // Networking Logic/read
            lock (MessageQueue)
            {
                for (int i = 0; i < MessageQueue.Count; i++)
                {
                    HandleServerMessage(MessageQueue.Dequeue());
                }
            }

            // Game Logic
        }

        private static void HandleServerMessage(NetIncomingMessage message)
        {
            switch (message.MessageType)
            {
                case NetIncomingMessageType.StatusChanged:
                    HandleConnectionPacket(message);
                    break;
                case NetIncomingMessageType.Data:
                    HandleDataPacketFromClient(message);
                    break;
                default:
                    Console.WriteLine($"{message.MessageType}: {message.ReadString()}");
                    break;
            }
        }

        private static void HandleDataPacketFromClient(NetIncomingMessage message)
        {
            var packetType = PacketTypes.Null;
            packetType = (PacketTypes)message.ReadInt32();

            switch (packetType)
            {
                case PacketTypes.Ping:
                    var pingId = message.ReadInt32();
                    SendPong(message.SenderConnection, pingId);
                    break;
                case PacketTypes.Pong:
                    // TODO Handle Ping/pong for server->client
                    break;
            }

            // If we don't read all the data in the packet
            if (message.Position != message.LengthBits)
                Console.WriteLine($"Malformed packet detected! {packetType}");
        }

        private static void HandleConnectionPacket(NetIncomingMessage message)
        {
            var connection = message.SenderConnection;

            switch (connection.Status)
            {
                case NetConnectionStatus.Connected:
                    var id = new Random().Next();

                    // Tell all players about this new connection
                    foreach (var c in Players)
                    {
                        var packet = CreatePacket(PacketTypes.ConnectionAdd);
                        packet.Write(id);

                        c.Key.SendMessage(packet, NetDeliveryMethod.ReliableOrdered, 0);
                    }

                    Players[connection] = new NetPlayer(connection);
                    Players[connection].ID = id;
                    connection.Approve(Server.CreateMessage($"Welcome to NetPlay, {Players[connection].ID}"));

                    // Send Welcome Packet that contains our ID and maybe some other info about the game/level/status etc
                    var welcomePacket = CreatePacket(PacketTypes.Welcome);
                    welcomePacket.Write(id);
                    connection.SendMessage(welcomePacket, NetDeliveryMethod.ReliableOrdered, 0);

                    break;
                case NetConnectionStatus.Disconnected:
                    var pid = Players[connection].ID;
                    Players.Remove(connection);

                    foreach (var c in Players)
                    {
                        var packet = CreatePacket(PacketTypes.ConnectionRemove);
                        packet.Write(pid);

                        c.Key.SendMessage(packet, NetDeliveryMethod.ReliableOrdered, 0);
                    }
                    break;
                default:
                    Console.WriteLine($"UNHANDLED CONNECTION STATE: {connection.Status}");
                    break;
            }
        }

        private static NetOutgoingMessage CreatePacket(PacketTypes msgType)
        {
            var m = Server.CreateMessage();
            m.Write((int)msgType);

            return m;
        }

        private static void SendPong(NetConnection connection, int pingId)
        {
            var m = CreatePacket(PacketTypes.Pong);
            m.Write(pingId);
            connection.SendMessage(m, NetDeliveryMethod.ReliableOrdered, 31);
        }

        private static void SendPing(NetConnection connection, int pingId)
        {
            var m = CreatePacket(PacketTypes.Ping);
            m.Write(pingId);
            connection.SendMessage(m, NetDeliveryMethod.ReliableOrdered, 31);
        }
        
    }
}
